// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require rails-ujs
//= require turbolinks
//= require trix
//= require_tree .
//= require jquery3
//= require popper
//= require select2
//= require bootstrap-sprockets
//= require jquery3
//= require popper
//= require bootstrap

persistentScrollsPositions = {};


$(document).on('turbolinks:before-visit', function() {
    var y = [window.scrollY];

    persistentScrollsPositions = y
    console.log(persistentScrollsPositions)
    return persistentScrollsPositions;
});



$(document).on('turbolinks:load', function() {
  document.documentElement.scrollTop = persistentScrollsPositions
  console.log(persistentScrollsPositions)
  return persistentScrollsPositions;
});



function myTRY() {
            var code = document.getElementById("editor").value;
            document.querySelector("iframe").contentWindow.document.body.innerHTML = code;
        } 


$(document).on("turbolinks:load",function(){
    $('#title').focus(function(){
        $('#exampletags').hide();
        $('#examplebody').hide();
        $('#exampletitle').slideDown("slow");
    });

    $('.formatted_content.trix-content.form-control').focus(function(){
        $('#exampletitle').hide();
        $('#exampletags').hide();
        $('#examplebody').slideDown("slow");
    });

    $('.tagss').click(function(){
        $('#exampletitle').hide();
        $('#examplebody').hide();
        $('#exampletags').slideDown("slow");
    });

    $('#question_question_tag_tag_id').select2({
        placeholder: "Por lo menos una etiqueta como (ruby react ruby-on-rails php laravel javascript)"
    });

    // $('pre').addClass('css')

    // // var element = document.createElement('code')
    // // $('pre').appendChild(element)


    // $('pre').each(function(i, block) {
    //     hljs.highlightBlock(block);
    // });

    // hljs.initHighlightingOnLoad()

    document.addEventListener("trix-change", function(event) {
      hola = (event.target.value)
      console.log(hola)
    }) 
    
    $('pre').each(function(i, block) {
        hljs.highlightBlock(block);
    });

    $('#searchButton').attr('disabled',true);
    $('#searchText').keyup(function(){
        if($(this).val().length !=0)
            $('#searchButton').attr('disabled', false);            
        else
            $('#searchButton').attr('disabled',true);
    })

    $(document).click("turbolinks:load", function(){
        $('pre').click(function(){
            $('pre').each(function(i, block) {
                hljs.highlightBlock(block);
            });
        })
    });
    

})


