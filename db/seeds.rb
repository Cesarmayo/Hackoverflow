# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
system('clear')
puts "-------------------".green
puts "|Creando Usuarios|".green
puts "-------------------".green

user = User.create! :first_name => 'Alfredo', :last_name => "Velasquez", :email => 'alfredo050398@gmail.com', :password => '123456', :password_confirmation => '123456'
user = User.create! :first_name => 'Rocco', :last_name => "De fina", :email => 'roccodefina12@gmail.com', :password => '123456', :password_confirmation => '123456'
user = User.create! :first_name => 'Jesus', :last_name => "Beltran", :email => 'jesusbeltran@gmail.com', :password => '123456', :password_confirmation => '123456'
user = User.create! :first_name => 'Romer', :last_name => "Ramos", :email => 'elromer@gmail.com', :password => '123456', :password_confirmation => '123456'


puts "-------------------".green
puts "|Creando Preguntas|".green
puts "-------------------".green

Question.create(title: "Tengo un problema background-color CSS", body: "Que pequeño el mundo es, que pequeño el mundo es, que pequeño el mundo es, que pequeño el mundo es, que pequeño el mundo es", user_id: 1)
Question.create(title: "Se me presento un problema bundle install en Ruby", body: "Bundle install es brutal, Bundle install es brutal, Bundle install es brutal, Bundle install es brutal, Bundle install es brutal, Bundle install es brutal", user_id: 1)
Question.create(title: "Tengo los conocimientos, pero no los cimientos", body: "Que pequeño el mundo es, que pequeño el mundo es, que pequeño el mundo es", user_id: 2)
Question.create(title: "Mañana iremos a la playa??", body: "Que pequeño el mundo es como dice la canción, Que pequeño el mundo es como dice la canción, Que pequeño el mundo es como dice la canción, Que pequeño el mundo es como dice la canción, Que pequeño el mundo es como dice la canción, Que pequeño el mundo es como dice la canción,", user_id: 2)
Question.create(title: "Hackoverflow creo que es mejor aplicacion que Stackoverflow", body: "Que pequeño el mundo es, que pequeño el mundo es, que pequeño el mundo es, Que pequeño el mundo es como dice la canción, Que pequeño el mundo es como dice la canción, ", user_id: 3)
Question.create(title: "Es mi octava vez instalando Ubuntu, quién soy?", body: "Que pequeño el mundo esQue pequeño el mundo es como dice la canción, Que pequeño el mundo es como dice la canción,Que pequeño el mundo es como dice la canción, Que pequeño el mundo es como dice la canción, Que pequeño el mundo es como dice la canción, Que pequeño el mundo es como dice la canción, Que pequeño el mundo es como dice la canción,", user_id: 3)
Question.create(title: "Trabajar en Haml es más cómodo que con Html??", body: "Que pequeño el mundo es, que pequeño el mundo es, que pequeño el mundo es Que pequeño el mundo es como dice la canción, Que pequeño el mundo es como dice la canción, Que pequeño el mundo es como dice la canción,", user_id: 4)
Question.create(title: "Tengo un problema con mi internet", body: "Que pequeño el mundo es, que pequeño el mundo es, que pequeño el mundo es Que pequeño el mundo es como dice la canción, Que pequeño el mundo es como dice la canción, Que pequeño el mundo es como dice la canción, Que pequeño el mundo es como dice la canción, Que pequeño el mundo es como dice la canción, Que pequeño el mundo es como dice la canción, Que pequeño el mundo es como dice la canción, Que pequeño el mundo es como dice la canción,", user_id: 4)


puts "-------------------".green
puts "|Creando Respuestas|".green
puts "-------------------".green

Answer.create(body: "Cualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equis", user_id: 1, question_id: 3)
Answer.create(body: "Cualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier", user_id: 1, question_id: 5)
Answer.create(body: "Cualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquierCualquier cosa equis", user_id: 2, question_id: 5)
Answer.create(body: "Cualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa eq equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equis", user_id: 2, question_id: 1)
Answer.create(body: "Cualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCua ccosa equis", user_id: 3, question_id: 1)
Answer.create(body: "Cualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equis", user_id: 3, question_id: 4)
Answer.create(body: "Cualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equis", user_id: 4, question_id: 5)
Answer.create(body: "Cualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equisCualquier cosa equis", user_id: 4, question_id: 2)

puts "-------------------".green
puts "|Creando Tags|".green
puts "-------------------".green

Tag.create(name:"Ruby")
Tag.create(name:"Ruby-on-rails")
Tag.create(name:"React")
Tag.create(name:"Javascript")
Tag.create(name:"Html")
Tag.create(name:"Css")
Tag.create(name:"Sass")
Tag.create(name:"Boostrap")
Tag.create(name:"Programación-estructurada")
Tag.create(name:"Condicionales")
Tag.create(name:"Iteraciones")
Tag.create(name:"Array")
Tag.create(name:"Hash")
Tag.create(name:"jQuery")
Tag.create(name:"Php")
Tag.create(name:"Laravel")
Tag.create(name:"GO")

puts "-------------------".green
puts "|Creando QuestionTags|".green
puts "-------------------".green

1.upto(8){|question_id|
    1.upto(rand(1..4)){QuestionTag.create(question_id: question_id, tag_id: rand(1..17))}
}